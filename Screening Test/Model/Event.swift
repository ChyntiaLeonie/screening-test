//
//  Event.swift
//  Screening Test
//
//  Created by Chyntia Leonie Andreas on 2/10/17.
//  Copyright © 2017 Chyntia Leonie Andreas. All rights reserved.
//

import UIKit

class Event: NSObject{
    var imageEvent : String = String()
    var nameEvent : String = String()
    var dateEvent : String = String()
}
