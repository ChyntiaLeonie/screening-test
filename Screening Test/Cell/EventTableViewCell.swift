//
//  EventTableViewCell.swift
//  Screening Test
//
//  Created by Chyntia Leonie Andreas on 2/10/17.
//  Copyright © 2017 Chyntia Leonie Andreas. All rights reserved.
//

import UIKit
class EventTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageEvent: UIImageView!
    
    @IBOutlet weak var nameEvent: UILabel!
    
    @IBOutlet weak var dateEvent: UILabel!
    
}